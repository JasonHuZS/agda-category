{-# OPTIONS --safe #-}

module CategoryTheory.Category where

open import Level renaming (suc to succ)
open import Relation.Binary
open import Function

record Category o m e : Set (succ (o ⊔ m ⊔ e)) where
  infixr 10 _↝_
  infixr 7 _∙_
  infix 4 _≈_
  
  field
    -- objects
    Obj : Set o
    -- morphisms
    _↝_ : Rel Obj m
    -- morphism composition
    _∙_ : ∀ {A B C} → B ↝ C → A ↝ B → A ↝ C
    -- identity morphism
    id↝ : ∀ {A} → A ↝ A

    -- we need to know how to compare two morphisms
    _≈_ : ∀ {A B} → Rel (A ↝ B) e
    ≈-isEquivalence : ∀ {A B} → IsEquivalence (_≈_ {A} {B})

    -- left/right identity laws
    id↝∙    : ∀ {A B} (f : A ↝ B) → id↝ ∙ f ≈ f
    ∙id↝    : ∀ {A B} (f : A ↝ B) → f ∙ id↝ ≈ f
    -- associativity of composition
    ∙-assoc : ∀ {A B C D} (f : A ↝ B) (g : B ↝ C) (h : C ↝ D) →
                 h ∙ g ∙ f ≈ (h ∙ g) ∙ f
    -- morphism composition preserves equivalence
    ∙-cong  : ∀ {A B C} {f₁ f₂ : A ↝ B} {g₁ g₂ : B ↝ C} →
                f₁ ≈ f₂ → g₁ ≈ g₂ →
                g₁ ∙ f₁ ≈ g₂ ∙ f₂

-- opposite category
_ᵒᵖ : ∀ {o m e} → Category o m e → Category _ _ _
cat ᵒᵖ = record
  { Obj             = Obj
  ; _↝_             = λ A B → B ↝ A
  ; _∙_             = λ f g → g ∙ f
  ; id↝             = id↝
  ; _≈_             = _≈_
  ; ≈-isEquivalence = ≈-isEquivalence
  ; id↝∙            = ∙id↝
  ; ∙id↝            = id↝∙
  ; ∙-assoc         = λ f g h → IsEquivalence.sym ≈-isEquivalence $ ∙-assoc h g f
  ; ∙-cong          = flip ∙-cong
  }
  where open Category cat
