{-# OPTIONS --safe #-}

module CategoryTheory.Functor where

open import Level using (_⊔_)
open import CategoryTheory.Category

record Functor {o₁ m₁ e₁ o₂ m₂ e₂}
               (𝓒₁ : Category o₁ m₁ e₁)
               (𝓒₂ : Category o₂ m₂ e₂) : Set (o₁ ⊔ m₁ ⊔ e₁ ⊔ o₂ ⊔ m₂ ⊔ e₂) where
  open Category 𝓒₁ using ()
    renaming (Obj to Obj₁ ; _↝_ to _↝₁_ ; _∙_ to _∙₁_ ; id↝ to id↝₁ ; _≈_ to _≈₁_)

  open Category 𝓒₂ using ()
    renaming (Obj to Obj₂ ; _↝_ to _↝₂_ ; _∙_ to _∙₂_ ; id↝ to id↝₂ ; _≈_ to _≈₂_)

  field
    -- functor maps objects from one category to another
    map  : Obj₁ → Obj₂
    -- functor maps morphisms to morphisms
    mmap : ∀ {A B} → A ↝₁ B → map A ↝₂ map B

    -- functor laws
    
    -- functor maps identities to identities
    map-id    : ∀ {A} → mmap (id↝₁ {A}) ≈₂ id↝₂ {map A}
    -- functor preserves composition
    map∙-homo : ∀ {A B C} → (f : A ↝₁ B) → (g : B ↝₁ C) →
                  mmap (g ∙₁ f) ≈₂ mmap g ∙₂ mmap f
    -- functor preserves morphism equivalence
    map-cong  : ∀ {A B} → (f g : A ↝₁ B) → f ≈₁ g →
                  mmap f ≈₂ mmap g
